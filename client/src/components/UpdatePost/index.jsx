import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon, Card, Label, Image } from 'semantic-ui-react';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import moment from 'moment';

import styles from './styles.module.scss';

const UpdatePost = ({
  post,
  updatePost,
  deletePost,
  uploadImage
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = moment(createdAt).fromNow();
  const [bodyGet, setBody] = useState(body);
  const [getImage, setImage] = useState(image);
  const [isUploading, setIsUploading] = useState(false);

  const handleUpdatePost = async () => {
    try {
      if (!bodyGet) {
        return;
      }
      await updatePost({ imageId: getImage ?.imageId, body: bodyGet, id });
    } catch (err) {
      NotificationManager.error(err.message);
    }
  };

  const handleDeletePost = async () => {
    try {
      await deletePost(id);
    } catch (err) {
      NotificationManager.error(err.message);
    }
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const response = await uploadImage(target.files[0]);
      const { id: imageId, link: imageLink } = response;
      setImage({ imageId, imageLink });
    } catch (err) {
      NotificationManager.error(err.message);
    } finally {
      setIsUploading(false);
    }
  };

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image ?.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Form>
          <Form.TextArea
            name="body"
            value={bodyGet}
            placeholder="What is the news?"
            onChange={ev => setBody(ev.target.value)}
          />
          <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
            <Icon name="image" />
            Attach image
            <input name="image" type="file" onChange={handleUploadFile} hidden />
          </Button>
          <Button floated="right" color="blue" type="submit" onClick={handleDeletePost}>Delete</Button>
          <Button floated="right" color="blue" type="submit" onClick={handleUpdatePost}>Update</Button>
        </Form>
        {getImage ?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={getImage ?.imageLink} alt="post" />
          </div>
        )}
      </Card.Content>
      <Card.Content extra>
        <Label basic size="small" as="a" className={styles.toolbarBtn}>
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn}>
          <Icon name="share alternate" />
        </Label>
      </Card.Content>
      <NotificationContainer />
    </Card>
  );
};

UpdatePost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  updatePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default UpdatePost;
