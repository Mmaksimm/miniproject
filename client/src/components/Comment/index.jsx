import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Form, Button } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import styles from './styles.module.scss';

const Comment = ({ comment: { body, createdAt, user, id },
  updateComment,
  edit,
  openCommentForEdit
}) => {
  const [bodyGet, setBody] = useState(body);

  const handleUpdateComment = async () => {
    try {
      if (!bodyGet) {
        return;
      }
      if (bodyGet === body) {
        openCommentForEdit(null);
        return;
      }
      await updateComment({ body: bodyGet, id });
    } catch (err) {
      NotificationManager.error(err.message);
    } finally {
      openCommentForEdit(null);
    }
  };

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        {edit
          ? (
            <Form>
              <Form.TextArea
                value={bodyGet}
                placeholder="Type a comment..."
                onChange={ev => setBody(ev.target.value)}
              />
              <Button
                content="Close"
                primary
                floated="left"
                onClick={() => openCommentForEdit(null)}
              />
              <Button
                content="Update"
                primary
                floated="right"
                onClick={() => handleUpdateComment()}
              />
            </Form>
          ) : (
            <CommentUI.Text onClick={() => openCommentForEdit(id)}>
              {bodyGet}
            </CommentUI.Text>
          )}
      </CommentUI.Content>
      <NotificationContainer />
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  updateComment: PropTypes.func.isRequired,
  edit: PropTypes.bool.isRequired,
  openCommentForEdit: PropTypes.func.isRequired
};

export default Comment;
