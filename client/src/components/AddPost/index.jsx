import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon, Image, Segment } from 'semantic-ui-react';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import styles from './styles.module.scss';

const AddPost = ({
  addPost,
  uploadImage
}) => {
  const [body, setBody] = useState('');
  const [image, setImage] = useState(undefined);
  const [isUploading, setIsUploading] = useState(false);

  const handleAddPost = async () => {
    if (!body) {
      return;
    }
    await addPost({ imageId: image ?.imageId, body });
    setBody('');
    setImage(undefined);
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const response = await uploadImage(target.files[0]);
      const { id: imageId, link: imageLink } = response;
      setImage({ imageId, imageLink });
    } catch (err) {
      // TODO: show error
      NotificationManager.error(Object.values(err)[1]);
    } finally {
      setIsUploading(false);
    }
  };

  return (
    <Segment>
      <Form onSubmit={handleAddPost}>
        <Form.TextArea
          name="body"
          value={body}
          placeholder="What is the news?"
          onChange={ev => setBody(ev.target.value)}
        />
        {image ?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image ?.imageLink} alt="post" />
          </div>
        )}
        <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
          <Icon name="image" />
          Attach image
          <input name="image" type="file" onChange={handleUploadFile} hidden />
        </Button>
        <Button floated="right" color="blue" type="submit">Post</Button>
      </Form>
      <NotificationContainer />
    </Segment>
  );
};

AddPost.propTypes = {
  addPost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default AddPost;
