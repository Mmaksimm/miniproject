import React, { useState } from 'react';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import { likePost, toggleExpandedPost, addComment } from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';
import UpdatePost from '../../components/UpdatePost';
import { updatePost, deletePost, updateComment } from './actions';

const ExpandedPost = ({
  userId,
  post,
  sharePost,
  expandedPostFor,
  likePost: like,
  toggleExpandedPost: toggle,
  addComment: add,
  updatePost: update,
  deletePost: deleted,
  updateComment: updatedComment
}) => {
  const [editCommentId, setEditCommentId] = useState(undefined);
  const uploadImage = file => imageService.uploadImage(file);

  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
      {post
        ? (
          <Modal.Content>
            {expandedPostFor === 'updatePost'
              ? (
                <UpdatePost
                  post={post}
                  updatePost={update}
                  deletePost={deleted}
                  uploadImage={uploadImage}
                />
              ) : (
                <Post
                  userId={userId}
                  post={post}
                  likePost={like}
                  toggleExpandedPost={() => { }}
                  sharePost={sharePost}
                />
              )}
            <CommentUI.Group style={{ maxWidth: '100%' }}>
              <Header as="h3" dividing>
                Comments
              </Header>
              {post.comments && post.comments
                .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                .map(comment => (
                  <Comment
                    key={comment.id}
                    comment={comment}
                    updateComment={updatedComment}
                    edit={comment.id === editCommentId}
                    userId={userId}
                    openCommentForEdit={userId === comment.user.id ? setEditCommentId : () => { }}
                  />
                ))}
              {<AddComment postId={post.id} addComment={add} />}
            </CommentUI.Group>
          </Modal.Content>
        )
        : <Spinner />}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  expandedPostFor: PropTypes.string,
  updatePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  updateComment: PropTypes.func.isRequired
};

ExpandedPost.defaultProps = {
  expandedPostFor: undefined
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  likePost,
  toggleExpandedPost,
  addComment,
  updatePost,
  deletePost,
  updateComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
