import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  UPDATE_POST,
  DELETE_POST,
  UPDATE_COMMENT
} from './actionTypes';

const updatePostAction = post => ({
  type: UPDATE_POST,
  post
});

export const updatePost = post => async dispatch => {
  const postUpdate = await postService.updatePost(post);
  dispatch(updatePostAction(postUpdate));
};

const deletePostAction = id => ({
  type: DELETE_POST,
  id
});

export const deletePost = id => async dispatch => {
  await postService.deletePost(id);
  dispatch(deletePostAction(id));
};

const updateCommentAction = comment => ({
  type: UPDATE_COMMENT,
  comment
});

export const updateComment = comment => async dispatch => {
  const commentUpdate = await commentService.updateComment(comment);
  dispatch(updateCommentAction(commentUpdate));
};
