import {
  SET_ALL_POSTS,
  LOAD_MORE_POSTS,
  ADD_POST,
  SET_EXPANDED_POST,
  UPDATE_POST,
  DELETE_POST,
  UPDATE_COMMENT
} from './actionTypes';

export default (state = {}, action) => {
  let index = null;
  let indexComment = null;
  switch (action.type) {
    case SET_ALL_POSTS:
      return {
        ...state,
        posts: action.posts,
        hasMorePosts: Boolean(action.posts.length)
      };
    case LOAD_MORE_POSTS:
      return {
        ...state,
        posts: [...(state.posts || []), ...action.posts],
        hasMorePosts: Boolean(action.posts.length)
      };
    case ADD_POST:
      return {
        ...state,
        posts: [action.post, ...state.posts]
      };
    case SET_EXPANDED_POST:
      return {
        ...state,
        expandedPost: action.expandedPost.post,
        expandedPostFor: action.expandedPost.expandedPostFor
      };
    case UPDATE_POST:
      index = state.posts.indexOf(state.posts.find(post => post.id === action.post.id));
      return {
        ...state,
        expandedPost: null,
        expandedPostFor: null,
        posts: [
          ...state.posts.slice(0, index),
          { ...action.post },
          ...state.posts.slice(index + 1)
        ]
      };
    case DELETE_POST:
      index = state.posts.indexOf(state.posts.find(post => post.id === action.id));
      return {
        ...state,
        expandedPost: null,
        expandedPostFor: null,
        posts: [
          ...state.posts.slice(0, index),
          ...state.posts.slice(index + 1)
        ]
      };
    case UPDATE_COMMENT:
      index = state.posts.indexOf(state.posts.find(post => post.id === action.comment.postId));
      indexComment = state
        .posts[index]
        .coments
        .indexOf(state.posts[index]
          .coments
          .find(post => post.id === action.coment.postId));
      return {
        ...state,
        editCommentId: null,
        posts: [
          ...state.posts.slice(0, index),
          {
            ...state.posts[index],
            coments: [
              ...state.posts[index].coments.slice(0, indexComment),
              { ...action.coment },
              ...state.posts[index].coments.slice(indexComment + 1)
            ]
          },
          ...state.posts.slice(index + 1)
        ]
      };
    default:
      return state;
  }
};
