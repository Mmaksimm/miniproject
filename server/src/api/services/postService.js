import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const updatePost = async (id, post) => {
  try {
    const success = await postRepository.updateById(id, post);

    if (!success) throw Error('Post update failed');

    const res = await getPostById(id);

    return res;
  } catch (err) {
    return Promise.reject({
      status: 500,
      message: err.message || 'Post update failed'
    });
  }
};

export const deletePost = async id => {
  try {
    const success = await postRepository.deleteById(id);
    if (!success) throw Error('Post delete failed');
    return { success: true };
  } catch (err) {
    return Promise.reject({
      status: 500,
      message: err.message || 'Post delete failed'
    });
  }
};

export const setReaction = async (userId, { postId, isLike }) => {
  const reaction = await postReactionRepository.getPostReaction(userId, postId);
  let isLiked = isLike;
  let postReact = null;

  const result = async () => {
    if (reaction && (isLike === reaction.isLike)) {
      await postReactionRepository.deleteById(reaction.id);
      return { isLiked };
    }

    if (!reaction) {
      postReact = await postReactionRepository.create({ userId, postId, isLike });
    }

    if (reaction && (isLike !== reaction.isLike)) {
      isLiked = !isLiked;
      postReact = await postReactionRepository.updateById(reaction.id, { isLike });
    }

    const post = await postRepository.getPostById(postReact.dataValues.postId);

    return {
      ...postReact.dataValues,
      post: post.dataValues,
      isLiked
    };
  };

  const response = result();
  return response;
};
