import commentRepository from '../../data/repositories/commentRepository';

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  userId
});

export const getCommentById = id => commentRepository.getCommentById(id);

export const updateComment = async (id, comment) => {
  try {
    const success = await commentRepository.updateById(id, comment);
    if (!success) throw Error('Comment update failed');
    const res = await getCommentById(id);
    return res;
  } catch (err) {
    return Promise.reject({
      status: 500,
      message: err.message || 'Comment update failed'
    });
  }
};
